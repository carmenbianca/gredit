gredit package
==============

Here is a rough UML class diagram.

.. image:: diagram.png

The signatures are incorrect because the UML program could not infer the types
from Python.  Just find the correct signatures below.

Submodules
----------

gredit.commands module
----------------------

.. automodule:: gredit.commands
    :members:
    :undoc-members:
    :show-inheritance:

gredit.core module
------------------

.. automodule:: gredit.core
    :members:
    :undoc-members:
    :show-inheritance:

gredit.models module
--------------------

.. automodule:: gredit.models
    :members:
    :undoc-members:
    :show-inheritance:

gredit.visitors module
----------------------

.. automodule:: gredit.visitors
    :members:
    :undoc-members:
    :show-inheritance:

gredit.widgets module
---------------------

.. automodule:: gredit.widgets
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gredit
    :members:
    :undoc-members:
    :show-inheritance:
