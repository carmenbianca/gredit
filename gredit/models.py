import random
from typing import Dict, Union

from PySide2.QtCore import QPoint, QRect, QSize, Qt
from PySide2.QtGui import QColor, QPainter, QPixmap

from .core import ShapeType
from .visitors import Visitor

ShapeLike = Union['Shape', 'ShapeGroup']


def item_to_object(item: Dict) -> ShapeLike:
    """Given a (json-ish) dictionary, convert it into an object.

    :param item: Json-ish dictionary.
    """
    if item.get('group') is not None:
        return ShapeGroup.fromDict(item)
    elif item.get('shape') is not None:
        return Shape.fromDict(item)


class Canvas:
    """Model to keep all shapes on.  This object doesn't do a whole lot other
    than storing data.
    """

    def __init__(self):
        self._shapes = []

    @classmethod
    def fromDict(cls, dictionary: Dict) -> 'Canvas':
        """:param dictionary: Json-ish dictionary."""
        canvas = cls()
        for item in dictionary['canvas']:
            canvas._shapes.append(item_to_object(item))
        return canvas

    def accept(self, visitor: Visitor) -> None:
        """Accept a visitor to perform some miscellaneous action.

        :param visitor: The visitor.
        """
        visitor.visitCanvas(self)

    def addShape(self, shape: ShapeLike) -> None:
        """Add a shape to the canvas.

        :param shape: Shape to add.
        """
        self._shapes.append(shape)

    def removeShape(self, shape: ShapeLike) -> None:
        """Remove a shape from the canvas.

        :param shape: Shape to remove.
        """
        self._shapes.remove(shape)

    def shapeAt(self, point: QPoint) -> ShapeLike:
        """Find the shape at *point*.

        :param point: Point to look at.
        """
        for shape in reversed(self._shapes):
            if shape.contains(point):
                return shape
        return None

    def __getitem__(self, index):
        return self._shapes[index]


class ShapeGroup(QRect):
    """A container for multiple shapes.  Can contain instances of
    :class:`ShapeGroup` as well.

    Note that it shares an interface with :class:`Shape`, but because Python
    doesn't do interfaces beyond inheritance, that might not be immediately
    obvious.

    .. IMPORTANT::

        This class is NOT COMPLETE.  Not all :class:`QRect` methods have been
        wrapped.  Only the necessary ones.

    .. NOTE::

        Inherits from :class:`QRect` and alters the behaviour of a couple of
        its methods.

        Technically *this* is a decorator pattern (inheriting from a class and
        altering the behaviour), and not the ornaments.

    :ivar QColor color: Color of the entire group.
    :ivar Dict[str,str] ornaments: Ornaments on the sides of the group.
    """

    def __init__(self):
        super().__init__()
        self._shapes = []
        self.color = QColor.fromRgb(
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255))
        self.ornaments = dict()

    @classmethod
    def fromDict(cls, dictionary) -> 'ShapeGroup':
        """:param dictionary: Json-ish dictionary."""
        group = cls()
        for item in dictionary['items']:
            group.addShape(item_to_object(item))
        group.ornaments = dictionary['ornaments']
        return group

    def accept(self, visitor: Visitor) -> None:
        """Accept a visitor to perform some miscellaneous action.

        :param visitor: The visitor.
        """
        visitor.visitShapeGroup(self)

    def addShape(self, shape: ShapeLike) -> None:
        """Add a shape to the internal structure.  Resize oneself to make sure
        that the shape will fit.

        :param shape: Shape to add.
        """
        self._shapes.append(shape)
        self._fitShapes()

    def moveTo(self, destination: QPoint) -> None:
        """Move to *destination*.  Also move the children to move there.

        :param destination: Top-left point to move to.
        """
        for shape in self._shapes:
            shape.moveTo(shape.topLeft() + (destination - self.topLeft()))
        super().moveTo(destination)
        self._fitShapes()

    def setSize(self, size: QSize) -> None:
        """Adjust the size to *size*, as well as the relative size of the
        children.

        When the children are resized, some precision is lost.  When resizing
        to a size, and then back to the old size, it might be possible that the
        children do not retain their exact original size because of a rounding
        error.

        :param size: Size to resize to.
        """
        width_multiplier = size.width() / self.width()
        height_multiplpier = size.height() / self.height()

        for shape in self._shapes:
            # Adjust the size and position of all children.
            shape.setSize(QSize(
                shape.width() * width_multiplier,
                shape.height() * height_multiplpier))

            shape.moveTo(QPoint(
                self.left() + (shape.left() - self.left()) * width_multiplier,
                self.top() + (shape.top()- self.top()) * height_multiplpier))
        self._fitShapes()

    def contains(self, point: QPoint) -> bool:
        """Overloaded method of :class:`QRect` that adjusts its behaviour.
        When asking whether *point* is within its own bounds, only return
        :const:`True` when a child contains it.

        Practically, this means that clicking on an "empty" space within
        :class:`ShapeGroup` will return :const:`False`, which might be
        unpredictable behaviour.

        :param point: A two-dimensional point.
        :result: Whether or not the point is contained by a child of self.
        """
        for shape in self._shapes:
            if shape.contains(point):
                return True
        return False

    def toPixmap(self, color: QColor = None) -> QPixmap:
        """Paint oneself onto a :class:`QPixmap` using *color* as brush.  All
        children are painted onto this pixmap as well, using the same color.

        The empty space on the pixmap is transparent.

        :param color: Color to use as brush.
        :result: A pixmap of the group.
        """
        if color is None:
            color = self.color
        pixmap = QPixmap(self.size())
        pixmap.fill(Qt.transparent)
        if pixmap:
            pixmap.painter = QPainter(pixmap)
            pixmap.painter.setPen(Qt.black)
            pixmap.painter.setBrush(color)

            for shape in self._shapes:
                rect = QRect(
                    shape.topLeft() - self.topLeft(),
                    shape.topLeft() - shape.bottomRight())
                pixmap.painter.drawPixmap(rect, shape.toPixmap(color=color))
            pixmap.painter.end()
        return pixmap

    def _fitShapes(self):
        """Make sure that our own dimensions match our children's."""
        left = None
        right = None
        top = None
        bottom = None

        for shape in self._shapes:
            if left is None or shape.left() < left:
                left = shape.left()
            if right is None or shape.right() > right:
                right = shape.right()
            if top is None or shape.top() < top:
                top = shape.top()
            if bottom is None or shape.bottom() > bottom:
                bottom = shape.bottom()
        self.setCoords(left, top, right, bottom)

    def __hash__(self):
        return id(self)


def _drawEllipse(painter, left, top, width, height) -> None:
    painter.drawEllipse(left, top, width, height)


def _drawRectangle(painter, left, top, width, height) -> None:
    painter.drawRect(left, top, width, height)


class Shape(QRect):
    """A shape with dimensions and a point on a two-dimensional space.  The
    shape is either an ellipse or a rectangle.

    :ivar ShapeType shapeType: Whether the shape is an ellipse or rectangle.
    :ivar QColor color: Color of the shape.
    :ivar Dict[str,str] ornaments: Ornaments on the sides of the shape.

    :param topLeft: Position of the shape.
    :param size: Size of the shape.
    :param shapeType: Whether the shape is an ellipse or rectangle.
    """

    def __init__(self, topLeft: QPoint, size: QSize, shapeType: ShapeType):
        super().__init__(topLeft, size)
        self.shapeType = shapeType
        self.color = QColor.fromRgb(
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255))
        self.ornaments = dict()

        if shapeType == ShapeType.ELLIPSE:
            self._drawStrat = _drawEllipse
        elif shapeType == ShapeType.RECTANGLE:
            self._drawStrat = _drawRectangle

    @classmethod
    def fromDict(cls, dictionary: Dict) -> 'Shape':
        """:param dictionary: Json-ish dictionary."""
        result = cls(
            QPoint(dictionary['left'], dictionary['top']),
            QSize(dictionary['width'], dictionary['height']),
            ShapeType(dictionary['shape']))
        result.ornaments = dictionary['ornaments']
        return result

    def accept(self, visitor: Visitor) -> None:
        """Accept a visitor to perform some miscellaneous action.

        :param visitor: The visitor.
        """
        visitor.visitShape(self)

    def toPixmap(self, color: QColor = None) -> QPixmap:
        """Paint oneself onto a :class:`QPixmap` using *color* as brush.

        The empty space on the pixmap is transparent.

        :param color: Color to use as brush.
        :result: A pixmap of the shape.
        """
        if color is None:
            color = self.color
        pixmap = QPixmap(self.size())
        pixmap.fill(Qt.transparent)
        if pixmap:
            pixmap.painter = QPainter(pixmap)
            pixmap.painter.setPen(Qt.black)
            pixmap.painter.setBrush(color)

            # Need to draw at (0, 0) here, or the whole thing becomes fucky.
            self._drawStrat(
                pixmap.painter,
                0, 0,
                self.size().width() - 1,
                self.size().height() - 1)
            pixmap.painter.end()
        return pixmap

    def __hash__(self):
        return id(self)
