import enum

from PySide2.QtWidgets import QUndoStack


class ClickMode(enum.Enum):
    """Possible modes to interact with the draw area."""

    DRAWING = 1
    SELECTING = 2


class ShapeType(enum.Enum):
    """Possible shapes to draw on the canvas."""

    ELLIPSE = 'ellipse'
    RECTANGLE = 'rectangle'


class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


class Settings(metaclass=Singleton):
    """Global settings object implemented as singleton.

    :ivar ClickMode clickMode: Currently selected click mode.
    :ivar ShapeType shapeType: Currently selected shape type.
    :ivar QUndoStack commands: Stack of commands that can be undone (and redone).
    :ivar str side: Currently selected side for adornments.
    """

    def __init__(self):
        self.clickMode = ClickMode.DRAWING
        self.shapeType = ShapeType.ELLIPSE
        self.commands = QUndoStack()
        self.side = 'left'
