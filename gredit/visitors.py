import json
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Sequence, Union

from . import models


class Visitor(metaclass=ABCMeta):
    """Abstract base class for the visitor pattern.

    Rather than having a single ``visit`` method, multiple methods are
    defined.  This is because Python does not simply support method
    overloading.
    """

    @abstractmethod
    def visitCanvas(self, canvas: 'models.Canvas') -> None:
        """:param canvas: Canvas to visit."""

    @abstractmethod
    def visitShapeGroup(self, group: 'models.ShapeGroup') -> None:
        """:param group: Group to visit."""

    @abstractmethod
    def visitShape(self, shape: 'models.ShapeGroup') -> None:
        """:param shape: Shape to visit."""


def _walk_container(
        container: Union[Sequence, Dict], keys: Sequence[Any]) -> Any:
    # Implementation speaks for itself.  Any description here would be longer
    # than the implementation.
    result = container
    for key in keys:
        result = result[key]
    return result


class ToJsonVisitor(Visitor):
    """Visit an object and convert it to json.

    Visiting an object more than once results in undefined behaviour.

    >>> canvas = Canvas()
    >>> visitor = ToJsonVisitor()
    >>> canvas.accept(visitor)
    >>> result = visitor.toJson()
    """

    def __init__(self):
        super().__init__()
        self._result = {}
        self._keys = []

    def toJson(self) -> str:
        """Return a json-string of the parsed objects."""
        return json.dumps(self._result, indent=True)

    def visitCanvas(self, canvas: 'models.Canvas') -> None:
        myList = []
        _walk_container(self._result, self._keys)['canvas'] = myList
        self._keys.append('canvas')

        for i, shape in enumerate(canvas):
            myList.append(None)
            self._keys.append(i)
            shape.accept(self)
            self._keys.pop()
        self._keys.pop()

    def visitShapeGroup(self, group: 'models.ShapeGroup') -> None:
        myList = []
        myGroup = {
            'group': len(group._shapes),
            'ornaments': group.ornaments,
            'items': myList}

        parent = _walk_container(self._result, self._keys[:-1])
        parent[self._keys[-1]] = myGroup
        self._keys.append('items')

        for i, shape in enumerate(group._shapes):
            myList.append(None)
            self._keys.append(i)
            shape.accept(self)
            self._keys.pop()
        self._keys.pop()

    def visitShape(self, shape: 'models.Shape') -> None:
        myShape = {
            'shape': shape.shapeType.value,
            'left': shape.left(),
            'top': shape.top(),
            'width': shape.width(),
            'height': shape.height(),
            'ornaments': shape.ornaments,
        }

        parent = _walk_container(self._result, self._keys[:-1])
        parent[self._keys[-1]] = myShape
