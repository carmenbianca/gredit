import json
from typing import Iterable

from PySide2.QtCore import QPoint, QSize
from PySide2.QtWidgets import QUndoCommand

from .models import Canvas, Shape, ShapeGroup, ShapeLike
from .visitors import ToJsonVisitor


class MoveCommand(QUndoCommand):
    """Command to move *shape* to *destination*.

    :param shape: Shape to move.
    :param destination: Coordinates to move *shape* to.
    """

    def __init__(self, shape: ShapeLike, destination: QPoint):
        super().__init__()
        self.shape = shape
        self.destination = destination
        self.origin = shape.topLeft()

    def redo(self):
        self.shape.moveTo(self.destination)

    def undo(self):
        self.shape.moveTo(self.origin)


class DrawCommand(QUndoCommand):
    """Command to add *shape* to *canvas*.

    :param shape: Shape to draw.
    :param canvas: Canvas to add *shape* to.
    """

    def __init__(self, shape: ShapeLike, canvas: 'Canvas'):
        super().__init__()
        self.shape = shape
        self.canvas = canvas

    def redo(self):
        self.canvas.addShape(self.shape)

    def undo(self):
        self.canvas.removeShape(self.shape)


class ResizeCommand(QUndoCommand):
    """Command to resize *shape* to *newSize*.

    :param shape: Shape to resize.
    :param newSize: New size of *shape*.
    """

    def __init__(self, shape: ShapeLike, newSize: QSize):
        super().__init__()
        self.shape = shape
        self.newSize = newSize
        self.oldSize = shape.size()

    def redo(self):
        self.shape.setSize(self.newSize)

    def undo(self):
        self.shape.setSize(self.oldSize)


class SaveCommand(QUndoCommand):
    """Command to save *shape* to *fileName*.

    :param canvas: Canvas to save.
    :param fileName: Path to save *canvas* to.
    """

    def __init__(self, canvas: 'Canvas', fileName: str):
        super().__init__()
        self.canvas = canvas
        self.fileName = fileName

    def redo(self):
        visitor = ToJsonVisitor()
        self.canvas.accept(visitor)
        with open(self.fileName, 'w') as fp:
            fp.write(visitor.toJson())

    def undo(self):
        # This is weird and useless.  You do not want to undo I/O.
        pass


class OpenCommand(QUndoCommand):
    """Command to open *fileName* into *canvas*.

    :param canvas: Canvas to load the file into.
    :param fileName: Path to file to open.
    """

    def __init__(self, canvas: 'Canvas', fileName: str):
        super().__init__()
        self.canvas = canvas
        self.fileName = fileName

    def redo(self):
        with open(self.fileName) as fp:
            canvas = Canvas.fromDict(json.load(fp))
        # Cheekily transfer the contents of the new canvas to the old one.
        self.canvas._shapes = canvas._shapes

    def undo(self):
        # This is weird and useless.  You do not want to undo I/O.
        pass


class GroupCommand(QUndoCommand):
    """Command to group the shapes in *group* together onto *canvas*.

    :param group: Iterable of shapes to add to *canvas*.
    :param canvas: Canvas to add onto.
    """

    def __init__(self, group: Iterable[ShapeLike], canvas: 'Canvas'):
        super().__init__()
        self.group = group
        self.canvas = canvas

        self._shapeGroup = ShapeGroup()
        for shape in self.group:
            self._shapeGroup.addShape(shape)

    def redo(self):
        for shape in self.group:
            self.canvas.removeShape(shape)
        self.canvas.addShape(self._shapeGroup)

    def undo(self):
        self.canvas.removeShape(self._shapeGroup)
        for shape in self.group:
            self.canvas.addShape(shape)


class OrnamentCommand(QUndoCommand):
    """Command to add an ornament to *shape* at *side*.

    :param shape: Shape to add ornament to.
    :param text: Text to use as ornament.
    :param side: Side of *shape* to add ornament to.
    """

    def __init__(self, shape: ShapeLike, text: str, side: str):
        super().__init__()
        self.shape = shape
        self.text = text
        self.side = side
        self._origText = self.shape.ornaments.get(side)

    def redo(self):
        if self.text:
            self.shape.ornaments[self.side] = self.text
        else:
            if self.shape.ornaments.get(self.side):
                del self.shape.ornaments[self.side]

    def undo(self):
        if self._origText:
            self.shape.ornaments[self.side] = self._origText
        else:
            del self.shape.ornaments[self.side]
