from pathlib import Path

from PySide2.QtCore import QPoint, QSize, Qt, Signal, Slot
from PySide2.QtGui import QColor, QMouseEvent, QPainter, QPalette
from PySide2.QtWidgets import (QAbstractButton, QAction, QButtonGroup,
                               QFileDialog, QGridLayout, QHBoxLayout,
                               QLineEdit, QMainWindow, QMenu, QMenuBar,
                               QPushButton, QRadioButton, QWidget)

from .commands import (DrawCommand, GroupCommand, MoveCommand, OpenCommand,
                       OrnamentCommand, ResizeCommand, SaveCommand)
from .core import ClickMode, Settings, ShapeType
from .models import Canvas, Shape, ShapeGroup


class DrawArea(QWidget):
    """Main widget for all drawing-related things.  This class mostly handles
    rendering the canvas and clicking to alter the canvas.  The canvas serves
    as a sort-of-but-not-really model, in that :class:`DrawArea` stores data in
    it and reads from it every time it renders.

    :param canvas: Internal canvas to use as model.
    :param parent: Parent widget.
    """

    #: Signal to emit when the selected shapes can be grouped up or not.
    groupChanged = Signal(bool)
    #: Signal to emit when a single shape is selected.
    selectChanged = Signal(bool)

    def __init__(self, canvas: Canvas, parent: QWidget = None):
        super().__init__(parent=parent)
        self._canvas = canvas

        # White background.
        palette = QPalette()
        palette.setColor(QPalette.Background, Qt.white)
        self.setPalette(palette)
        self.setAutoFillBackground(True)

        self._drawing = False
        self._selectedShape = None
        self._selectedAt = None
        self._groupingShapes = set()

    def mousePressEvent(self, event: QMouseEvent) -> None:
        """Handle *event*.  Depending on the :class:`ClickMode` of
        :class:`Settings`, the following happens:

        - If drawing and clicked with left mouse button, internally store where
          we have started drawing.

        - If selecting and clicked with left mouse button, internally select
          the shape (or unselect if no shape is clicked on).

        - If selecting and clicked with right mouse button, select the shape
          for resizing.

        :param event: Mouse event.
        """
        pos = event.pos()

        if event.button() == Qt.LeftButton:
            if Settings().clickMode == ClickMode.DRAWING:
                self._drawing = True
                self._drawStartPos = pos
                self._drawCurrentPos = pos

            if Settings().clickMode == ClickMode.SELECTING:
                self._selectedShape = self._canvas.shapeAt(pos)
                self._selectedAt = pos
                if not self._selectedShape:
                    self._groupingShapes = set()
                    self._groupChangedEmit()

        if event.button() == Qt.RightButton:
            if Settings().clickMode == ClickMode.SELECTING:
                self._selectedShape = self._canvas.shapeAt(pos)
                self._selectedAt = pos

        self.update()

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        """Handle *event*.  Currently this function does not do a lot.  Ideally
        this function would update the rendering to show what the user is doing
        while moving the mouse across the draw area, but that is a lot of
        work, so it is only implemented for drawing.

        :param event: Mouse event.
        """
        pos = event.pos()

        if event.buttons() & Qt.LeftButton:
            if Settings().clickMode == ClickMode.DRAWING:
                self._drawCurrentPos = pos

            if Settings().clickMode == ClickMode.SELECTING:
                # TODO: Hint that something is being moved
                pass
                # if self._selectedShape:
                #     self._selectedShape.x += pos.x() - self._selectedAt.x()
                #     self._selectedShape.y += pos.y() - self._selectedAt.y()
                #     self._selectedAt = pos

        # TODO: Make this nicer
        # elif event.buttons() & Qt.RightButton:
        #     if Settings().clickMode == ClickMode.SELECTING:
        #         if self._selectedShape:
        #             replacement = Shape(
        #                 self._selectedShape.topLeft(),
        #                 QSize(
        #                     max(5, self._selectedShape.width()
        #                         + (pos.x() - self._selectedAt.x())),
        #                     max(5, self._selectedShape.height()
        #                         + (pos.y() - self._selectedAt.y()))),
        #                 self._selectedShape.shapeType)
        #             self._canvas.replaceShape(self._selectedShape, replacement)
        #             self._selectedShape = replacement
        #             self._selectedAt = pos

        self.update()

    def mouseReleaseEvent(self, event: QMouseEvent) -> None:
        """Handle *event*.  This finalises all actions started in
        :meth:`mousePressEvent`.

        - If drawing and clicked with left mouse button, draw the shape on the
          canvas.

        - If selecting and clicked with left mouse button, and the mouse has
          not been moved, mark the shape as selected for the outside world.

        - If selecting and clicked with left mouse button, and the mouse has
          moved, move the shape on the canvas.

        - If selecting and clicked with right mouse button, resize the shape on
          the canvas.

        :param event: Mouse event.
        """
        pos = event.pos()

        if event.button() == Qt.LeftButton:
            if Settings().clickMode == ClickMode.DRAWING:
                self._drawing = False
                shape = Shape(
                    QPoint(
                        min(pos.x(), self._drawStartPos.x()),
                        min(pos.y(), self._drawStartPos.y())),
                    QSize(
                        abs(pos.x() - self._drawStartPos.x()),
                        abs(pos.y() - self._drawStartPos.y())),
                    Settings().shapeType)
                command = DrawCommand(shape, self._canvas)
                Settings().commands.push(command)

            if Settings().clickMode == ClickMode.SELECTING:
                if self._selectedShape:

                    # Move
                    if self._selectedAt != pos:
                        origin = self._selectedShape.topLeft()
                        command = MoveCommand(
                            self._selectedShape,
                            pos - (self._selectedAt - origin))
                        Settings().commands.push(command)

                    # Select for group
                    else:
                        if self._selectedShape in self._groupingShapes:
                            self._groupingShapes.remove(self._selectedShape)
                            self._groupChangedEmit()
                        else:
                            self._groupingShapes.add(self._selectedShape)
                            self._groupChangedEmit()

        elif event.button() == Qt.RightButton:
            if Settings().clickMode == ClickMode.SELECTING:
                if self._selectedShape:
                    new_size = QSize(
                        max(5, self._selectedShape.width()
                            + (pos.x() - self._selectedAt.x())),
                        max(5, self._selectedShape.height()
                            + (pos.y() - self._selectedAt.y())))
                    command = ResizeCommand(self._selectedShape, new_size)
                    Settings().commands.push(command)

        self.update()
        self._selectedShape = None
        self._selectedAt = None

    def _groupChangedEmit(self) -> None:
        self.groupChanged.emit(len(self._groupingShapes) >= 2)
        self.selectChanged.emit(len(self._groupingShapes) == 1)

    def paintEvent(self, event) -> None:
        """Handle *event*.  Really this should be a private method."""
        self._drawShapes()
        self._drawOrnaments()

        # TODO: Show drawing hint
        if self._drawing:
            draw_painter = QPainter(self)
            pos = self._drawCurrentPos
            shape = Shape(
                QPoint(
                    min(pos.x(), self._drawStartPos.x()),
                    min(pos.y(), self._drawStartPos.y())),
                QSize(
                    abs(pos.x() - self._drawStartPos.x()),
                    abs(pos.y() - self._drawStartPos.y())),
                Settings().shapeType)
            draw_painter.drawPixmap(shape, shape.toPixmap(color=Qt.green))
            draw_painter.end()

    def _drawShapes(self):
        painter = QPainter(self)
        for shape in self._canvas:
            color = None
            if shape in self._groupingShapes:
                color = QColor.fromRgb(255, 255, 255)
            painter.drawPixmap(shape.topLeft(), shape.toPixmap(color=color))
        painter.end()

    def _drawOrnaments(self):
        painter = QPainter(self)

        def _iterate(shape):
            if isinstance(shape, ShapeGroup):
                for item in shape._shapes:
                    _iterate(item)
            for side, ornament in shape.ornaments.items():
                _draw(shape, side, ornament)

        def _draw(shape, side, ornament):
            point = shape.center()
            # Annoying...
            if side == 'left':
                point.setX(shape.left())
            elif side == 'right':
                point.setX(shape.right())
            elif side == 'top':
                point.setY(shape.top())
            elif side == 'bottom':
                point.setY(shape.bottom())
            painter.drawText(point, ornament)

        for shape in self._canvas:
            _iterate(shape)

        painter.end()

    @Slot()
    def group(self) -> None:
        """Combine all selected shapes into a group on the canvas."""

        if self._groupingShapes:
            command = GroupCommand(self._groupingShapes, self._canvas)
            Settings().commands.push(command)
            self._groupingShapes = set()
            self._groupChangedEmit()
            self.update()


class MainWidget(QWidget):
    """The main widget that contains everything.  Technically this widget might
    as well be split up into tinier components, but there is no real gain
    there, because all UI components are intrinsically fairly tightly coupled
    anyway.

    :param canvas: Canvas to store internally.
    :param parent: Parent widget.
    """

    def __init__(self, canvas: Canvas, parent: QWidget = None):
        super().__init__(parent=parent)

        self._canvas = canvas

        # I should have done this in a .ui file...
        gridLayout = QGridLayout()

        hbox = QHBoxLayout()
        self._drawRadio = QRadioButton('&Draw', self)
        self._drawRadio.setChecked(True)
        self._selectRadio = QRadioButton('&Select', self)
        self._radioGroup = QButtonGroup(self)
        self._radioGroup.addButton(self._drawRadio)
        self._radioGroup.addButton(self._selectRadio)
        hbox.addWidget(self._drawRadio)
        hbox.addWidget(self._selectRadio)
        gridLayout.addLayout(hbox, 0, 0)

        self._drawArea = DrawArea(canvas)
        gridLayout.addWidget(self._drawArea, 1, 0)

        hbox = QHBoxLayout()
        self._ellipseRadio = QRadioButton('&Ellipse', self)
        self._ellipseRadio.setChecked(True)
        self._rectRadio = QRadioButton('&Rectangle', self)
        self._sizeRadioGroup = QButtonGroup(self)
        self._sizeRadioGroup.addButton(self._ellipseRadio)
        self._sizeRadioGroup.addButton(self._rectRadio)
        hbox.addWidget(self._ellipseRadio)
        hbox.addWidget(self._rectRadio)
        gridLayout.addLayout(hbox, 2, 0)

        hbox = QHBoxLayout()
        self._undoButton = QPushButton('Undo', self)
        self._undoButton.setEnabled(False)
        self._redoButton = QPushButton('Redo', self)
        self._redoButton.setEnabled(False)
        hbox.addWidget(self._undoButton)
        hbox.addWidget(self._redoButton)
        gridLayout.addLayout(hbox, 3, 0)

        self._groupButton = QPushButton('Create group', self)
        self._groupButton.setEnabled(False)
        gridLayout.addWidget(self._groupButton, 4, 0)

        hbox = QHBoxLayout()
        self._ornamentEdit = QLineEdit(self)
        self._ornamentButton = QPushButton('Set text', self)
        self._ornamentButton.setEnabled(False)
        hbox.addWidget(self._ornamentEdit)
        hbox.addWidget(self._ornamentButton)
        gridLayout.addLayout(hbox, 5, 0)

        hbox = QHBoxLayout()
        self._leftRadio = QRadioButton('Left', self)
        self._leftRadio.setChecked(True)
        self._rightRadio = QRadioButton('Right', self)
        self._topRadio = QRadioButton('Top', self)
        self._bottomRadio = QRadioButton('Bottom', self)
        self._sideRadioGroup = QButtonGroup(self)
        self._sideRadioGroup.addButton(self._leftRadio)
        self._sideRadioGroup.addButton(self._rightRadio)
        self._sideRadioGroup.addButton(self._topRadio)
        self._sideRadioGroup.addButton(self._bottomRadio)
        hbox.addWidget(self._leftRadio)
        hbox.addWidget(self._rightRadio)
        hbox.addWidget(self._topRadio)
        hbox.addWidget(self._bottomRadio)
        gridLayout.addLayout(hbox, 6, 0)

        self.setLayout(gridLayout)

        self._radioGroup.buttonToggled.connect(self._setDrawMode)
        self._sizeRadioGroup.buttonToggled.connect(self._setShapeMode)
        self._sideRadioGroup.buttonToggled.connect(self._setSide)
        Settings().commands.canUndoChanged.connect(self._changeUndo)
        Settings().commands.canRedoChanged.connect(self._changeRedo)
        self._undoButton.clicked.connect(self._undo)
        self._redoButton.clicked.connect(self._redo)
        self._drawArea.groupChanged.connect(self._changeGroup)
        self._drawArea.selectChanged.connect(self._changeSelect)
        self._groupButton.clicked.connect(self._drawArea.group)
        self._ornamentButton.clicked.connect(self._addOrnament)

    @Slot(QAbstractButton, bool)
    def _setDrawMode(self, button, checked) -> None:
        if checked:
            if button == self._drawRadio:
                Settings().clickMode = ClickMode.DRAWING
            elif button == self._selectRadio:
                Settings().clickMode = ClickMode.SELECTING

    @Slot(QAbstractButton, bool)
    def _setShapeMode(self, button, checked) -> None:
        if checked:
            if button == self._ellipseRadio:
                Settings().shapeType = ShapeType.ELLIPSE
            elif button == self._rectRadio:
                Settings().shapeType = ShapeType.RECTANGLE

    @Slot(QAbstractButton, bool)
    def _setSide(self, button, checked) -> None:
        if checked:
            # Can probably use an enum instead of magic strings, but this will
            # suffice
            if button == self._leftRadio:
                Settings().side = 'left'
            elif button == self._rightRadio:
                Settings().side = 'right'
            elif button == self._topRadio:
                Settings().side = 'top'
            elif button == self._bottomRadio:
                Settings().side = 'bottom'

    @Slot(bool)
    def _changeUndo(self, canUndo) -> None:
        self._undoButton.setEnabled(canUndo)

    @Slot(bool)
    def _changeRedo(self, canRedo) -> None:
        self._redoButton.setEnabled(canRedo)

    @Slot(bool)
    def _changeGroup(self, canGroup) -> None:
        self._groupButton.setEnabled(canGroup)

    @Slot(bool)
    def _changeSelect(self, canSelect) -> None:
        self._ornamentButton.setEnabled(canSelect)

    @Slot()
    def _undo(self) -> None:
        Settings().commands.undo()
        self._drawArea.update()

    @Slot()
    def _redo(self) -> None:
        Settings().commands.redo()
        self._drawArea.update()

    @Slot()
    def _addOrnament(self) -> None:
        text = self._ornamentEdit.text()
        side = Settings().side
        # Very sneaky
        shape = list(self._drawArea._groupingShapes)[0]
        command = OrnamentCommand(shape, text, side)
        Settings().commands.push(command)
        self._drawArea.update()


class MainWindow(QMainWindow):
    """The main window that contains the :class:`MainWidget` and little else.
    It also has a menu bar for loading and saving.

    :param parent: Parent widget.
    """

    def __init__(self, parent: QWidget = None):
        super().__init__(parent=parent)

        self._saveAction = QAction('Save', self)
        self._openAction = QAction('Open', self)

        self._canvas = Canvas()
        self._centralWidget = MainWidget(self._canvas, self)
        self.setCentralWidget(self._centralWidget)

        self._menubar = QMenuBar(self)
        self._fileMenu = self._menubar.addMenu('File')
        self._fileMenu.addAction(self._saveAction)
        self._fileMenu.addAction(self._openAction)
        self.setMenuBar(self._menubar)

        self.resize(500, 400)

        self._saveAction.triggered.connect(self._save)
        self._openAction.triggered.connect(self._open)

    @Slot()
    def _save(self) -> None:
        fileName = QFileDialog.getSaveFileName(
            self, 'Save file', str(Path('./data.json')))
        command = SaveCommand(self._canvas, fileName[0])
        command.redo()

    @Slot()
    def _open(self) -> None:
        fileName = QFileDialog.getOpenFileName(
            self, 'Open file', str(Path('./data.json')))
        command = OpenCommand(self._canvas, fileName[0])
        command.redo()
        # Extremely sneaky: Call update on grandchild.
        self._centralWidget._drawArea.update()
