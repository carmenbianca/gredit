import sys

from PySide2.QtWidgets import QApplication

from gredit.widgets import MainWindow


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())
